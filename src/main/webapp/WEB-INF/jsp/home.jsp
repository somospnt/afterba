<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>

<html lang="en">
    <head>
        <title>Proximamente - AfterBA</title>
        <meta charset="utf-8">
        <meta name = "format-detection" content = "telephone=no" />
        <link rel="icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="css/grid.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/touchTouch.css">
        <link rel="stylesheet" href="css/camera.css">
    </head>
    <body class="index">
        <header id="header">
            <div id="stuck_container">
                <div class="container">
                    <div class="row">
                        <div class="grid_12">
                            <div class="social">
                                <a href="#"><span class="bd-ra fa fa-facebook"></span></a>
                                <a href="#"><span class="bd-ra fa fa-tumblr"></span></a>
                                <a href="#"><span class="bd-ra fa fa-google-plus"></span></a>
                            </div>
                            <h1><a href="#map-canvas">AfterBA<span>DONDE EMPIEZA TU NOCHE</span></a></h1>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section id="content">
            <div class="full-width-container block-1">
                <div class="camera_container">
                    <div id="camera_wrap">
                        <div class="item" data-src="images/index_img_slider-1.png">
                            <div class="camera_caption fadeIn">
                                <h3>¿Estás re manija y buscas un after?</h3>
                            </div>
                        </div>
                        <div class="item" data-src="images/index_img_slider-1.png">
                            <div class="camera_caption fadeIn">
                                <h3>Proximamente...</h3>
                            </div>
                        </div>
                        <div class="item" data-src="images/index_img_slider-1.png">
                            <div class="camera_caption fadeIn">
                                <h3>Vas a encontrar los mejores de la noche porteña</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="full-width-container block-6">
                <div class="google-map-api"> 
                    <div id="map-canvas" class="gmap"></div> 
                </div> 
            </div>
        </section>

        <footer id="footer">
            <div class="footer_bottom">
                <div class="container">
                    <span id="copyright-year"></span> © <strong>Somos PNT</strong> - El equipo de desarrollo de <a href="http://www.connectis-ict.com.ar">Connectis Argentina</a>
                </div>
            </div>
        </footer>

        <script src="js/lib/jquery.js"></script>
        <script src="js/lib/jquery-migrate-1.2.1.js"></script>
        <script src='js/lib/camera.js'></script>
        <script src="js/lib/touchTouch.jquery.js"></script>
        <script src="js/lib/jquery.stellar.js"></script>
        <script src="js/script.js"></script>
        <script src='//maps.googleapis.com/maps/api/js?v=3.exp'></script>
        <!--[if (gt IE 9)|!(IE)]><!-->
        <script src="js/lib/jquery.mobile.customized.min.js"></script>
        <script src="js/lib/wow.js"></script>

        <script src="js/app/afterba.js"></script>
        <script src="js/app/service/service.js"></script>
        <script src="js/app/service/bar/bar.js"></script>
        <script src="js/app/service/geolocalizacion/geolocalizacion.js"></script>
        <script src="js/app/ui/ui.js"></script>
        <script src="js/app/ui/mapa/mapa.js"></script>

        <script>
            $(document).ready(function () {
                if ($('html').hasClass('desktop')) {
                    new WOW().init();
                }
            });
        </script>

        <script>
            jQuery(function () {
                jQuery('#camera_wrap').camera({
                    height: '68.125%',
                    thumbnails: false,
                    pagination: true,
                    fx: 'simpleFade',
                    loader: 'none',
                    hover: false,
                    navigation: false,
                    playPause: false,
                    minHeight: "980px"
                });
            });
        </script>
        <!--script>
                var $container = $('.isotope');
                // init
                $container.on( 'click', '.iso-item', function( event ) {
                  // change size of item via class
                  $( event.target ).toggleClass('gigante');
                  // trigger layout
                  $container.packery();
                  /*$container.packery({
                          itemSelector: 'iso-.item',
                          gutter: 10*/
        
                });
        </script-->
        <script>
            $(document).ready(function () {
                if ($('html').hasClass('desktop')) {
                    $.stellar({
                        horizontalScrolling: false,
                        verticalOffset: 20,
                        resposive: true,
                        hideDistantElements: true,
                    });
                }
            });
        </script>

        <script>
            $(function () {
                $('.isotope a').touchTouch();
            });
        </script>

        <script>
            $(document).ready(function () {
                afterba.ui.mapa.init();
            });
        </script>        

    </body>
</html>