package com.pnt.afterba.service.impl;

import com.pnt.afterba.domain.Bar;
import com.pnt.afterba.repository.BarRepository;
import com.pnt.afterba.service.BarService;
import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class BarServiceImpl implements BarService {

    @Autowired
    private BarRepository barRepository;

    @Override
    public void guardar(Bar bar) {
        if (bar.getNombre().isEmpty() ) {
            throw new IllegalArgumentException("Nombre vacio");
        }
        if(bar.getDireccion().isEmpty()){
            throw new IllegalArgumentException("Direccion vacia");
        }
        if (bar.getLatitud() == null || bar.getLongitud() == null) {
            throw new IllegalArgumentException("latitud o longitu null");
        }
        barRepository.save(bar);
    }

    @Override
    public List<Bar> buscarPorNombre(String nombreBar) {
        if (nombreBar == null) {
            throw new NullPointerException();
        }
        if (nombreBar.isEmpty() || nombreBar.trim().isEmpty()) {
            return new ArrayList<>();
        }
        return barRepository.findByNombreIgnoreCaseContaining(nombreBar);
    }

    @Override
    public List<Bar> obtenerTodos() {
        return barRepository.findAll();
    }

}
