afterba.service.geolocalizacion = (function () {

    function obtenerJSONGeolocalizacion(bar) {
        var url = "http://maps.googleapis.com/maps/api/geocode/json?address=";
        var direccion = bar.direccion.replace(" ", "+");
        var urlDireccion = url.concat(direccion);
        return afterba.service.get(urlDireccion);
    }
    return {
        obtenerJSONGeolocalizacion: obtenerJSONGeolocalizacion
    };
})();