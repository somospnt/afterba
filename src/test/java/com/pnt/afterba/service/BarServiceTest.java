package com.pnt.afterba.service;

import com.pnt.afterba.AfterBaApplicationTests;
import com.pnt.afterba.domain.Bar;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.jdbc.JdbcTestUtils;

public class BarServiceTest extends AfterBaApplicationTests {

    @Autowired
    private BarService barService;

    @Test
    public void guardarBar_conUbicacionValida_retornaTrue() {
        Bar bar = new Bar();

        bar.setLatitud(-341.0);
        bar.setLongitud(32.0);
        bar.setId(4L);
        bar.setNombre("Connectis");
        bar.setDireccion("Bulnes 2756,Palermo,CABA");

        int cantidadBaresAntes = JdbcTestUtils.countRowsInTable(jdbcTemplate, "bar");
        barService.guardar(bar);
        int cantidadBaresDespues = JdbcTestUtils.countRowsInTable(jdbcTemplate, "bar");
        assertEquals(cantidadBaresAntes + 1, cantidadBaresDespues);
    }

    @Test(expected = IllegalArgumentException.class)
    public void guardarBar_NombreVacio_IllegalArgumentException() {
        Bar bar = new Bar();

        bar.setLatitud(-341.0);
        bar.setLongitud(32.0);
        bar.setId(1L);
        bar.setNombre("");
        bar.setDireccion("Bulnes 2756,Palermo,CABA");

        int cantidadBaresAntes = JdbcTestUtils.countRowsInTable(jdbcTemplate, "bar");
        barService.guardar(bar);
        int cantidadBaresDespues = JdbcTestUtils.countRowsInTable(jdbcTemplate, "bar");
        assertEquals(cantidadBaresAntes, cantidadBaresDespues);
    }

    @Test(expected = IllegalArgumentException.class)
    public void guardarBar_ubicacionNula_IllegalArgumentException() {
        Bar bar = new Bar();

        bar.setId(1L);
        bar.setNombre("Connectis");
        bar.setDireccion("Bulnes 2756,Palermo,CABA");
        bar.setLatitud(null);
        bar.setLongitud(null);
        barService.guardar(bar);
    }

    @Test(expected = IllegalArgumentException.class)
    public void guardarBar_DireccionVacia_IllegalArgumentException() {
        Bar bar = new Bar();

        bar.setLatitud(-341.0);
        bar.setLongitud(32.0);
        bar.setId(1L);
        bar.setNombre("Connectis");
        bar.setDireccion("");

        int cantidadBaresAntes = JdbcTestUtils.countRowsInTable(jdbcTemplate, "bar");
        barService.guardar(bar);
        int cantidadBaresDespues = JdbcTestUtils.countRowsInTable(jdbcTemplate, "bar");
        assertEquals(cantidadBaresAntes, cantidadBaresDespues);
    }

    @Test
    public void buscarPorNombre_ConNombreValido_retornaListaBar() {
        String nombreBar = "ares";
        List<Bar> listaBares = barService.buscarPorNombre(nombreBar);

        assertEquals(1, listaBares.size());
    }

    @Test
    public void buscarPorNombre_ConNombreVacio_retornaListaBarVacia() {
        String nombreBar = "";
        List<Bar> listaBares = barService.buscarPorNombre(nombreBar);

        assertTrue(listaBares.isEmpty());
    }

    @Test
    public void buscarPorNombre_ConNombreEspacio_retornaListaBarVacia() {
        String nombreBar = " ";
        List<Bar> listaBares = barService.buscarPorNombre(nombreBar);

        assertTrue(listaBares.isEmpty());
    }

    @Test(expected = NullPointerException.class)
    public void buscarPorNombre_null_NullPointerException() {
        String nombreBar = null;
        barService.buscarPorNombre(nombreBar);
    }

    @Test
    public void buscarPorNombre_ConNombreInexistente_retornaListaBarVacia() {
        String nombreBar = "XXXX";
        List<Bar> listaBares = barService.buscarPorNombre(nombreBar);
        assertTrue(listaBares.isEmpty());
    }

    @Test
    public void buscarTodos_retornaListaBar() {
        List<Bar> listaBar = barService.obtenerTodos();
        int cantidadBares = JdbcTestUtils.countRowsInTable(jdbcTemplate, "bar");

        assertEquals(cantidadBares, listaBar.size());
    }

}
