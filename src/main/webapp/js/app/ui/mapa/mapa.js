afterba.ui.mapa = (function () {

    function init() {
        initMapa();
        initGuardar();
    }

    function initMapa() {
        var mapa;
        var bsAsLatLong = new google.maps.LatLng(parseFloat(-34.589964), parseFloat(-58.433204, 10));
        var estilos = [
            {"featureType": "water", "stylers": [{"color": "#000000"}]},
            {"featureType": "landscape", "stylers": [{"color": "#08304b"}]},
            {"featureType": "poi", "elementType": "geometry", "stylers": [{"color": "#0c4152"}, {"lightness": 5}]},
            {"featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{"color": "#000000"}]},
            {"featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{"color": "#0b434f"}, {"lightness": 25}]},
            {"featureType": "road.arterial", "elementType": "geometry.fill", "stylers": [{"color": "#000000"}]},
            {"featureType": "road.arterial", "elementType": "geometry.stroke", "stylers": [{"color": "#0b3d51"}, {"lightness": 16}]},
            {"featureType": "road.local", "elementType": "geometry", "stylers": [{"color": "#000000"}]},
            {"elementType": "labels.text.fill", "stylers": [{"color": "#ffffff"}]},
            {"elementType": "labels.text.stroke", "stylers": [{"color": "#000000"}, {"lightness": 13}]},
            {"featureType": "transit", "stylers": [{"color": "#146474"}]},
            {"featureType": "administrative", "elementType": "geometry.fill", "stylers": [{"color": "#000000"}]},
            {"featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{"color": "#144b53"}, {"lightness": 14}, {"weight": 1.4}]}
        ];

        function inicializar() {
            var opcionesMapa = {
                zoom: 13,
                center: bsAsLatLong,
                scrollwheel: false,
                styles: estilos
            };

            mapa = new google.maps.Map(document.getElementById("map-canvas"), opcionesMapa);
            var infowindow = new google.maps.InfoWindow();
            google.maps.event.addListener(mapa, 'click', function () {
                infowindow.close();
            });

            google.maps.event.addDomListener(window, 'resize', function () {
                mapa.setCenter(bsAsLatLong);
            });

            afterba.service.bar.obtenerTodos()
                    .success(obtenerTodosSuccess)
                    .fail(function () {
                        alert("Hubo un problema cargando los bares en el mapa");
                    });

            function obtenerTodosSuccess(data) {
                $.each(data, function (i, bar) {
                    var latLongBar = {lat: bar.latitud, lng: bar.longitud};
                    var puntoMapa = new google.maps.Marker({
                        mapa: mapa,
                        position: latLongBar,
                        draggable: false,
                        animation: google.maps.Animation.DROP,
                        title: bar.nombre
                    });

                    function desplegarInfo() {
                        infowindow.close();
                        if (puntoMapa.getAnimation() !== null) {
                            puntoMapa.setAnimation(null);
                        } else {
                            puntoMapa.setAnimation(google.maps.Animation.BOUNCE);
                            setTimeout(function(){ puntoMapa.setAnimation(null); }, 1430);
                        }
                        var infoBar =
                                '<div id="afterba-info-window">' +
                                    '<strong>' + bar.nombre + '</strong>' +
                                    '</br>' + bar.direccion +
                                '</div>';

                        infowindow.setContent(infoBar);
                        infowindow.open(mapa, puntoMapa);
                    }

                    puntoMapa.addListener('click', desplegarInfo);
                    puntoMapa.setMap(mapa);
                });
            }
        }

        google.maps.event.addDomListener(window, "load", inicializar);
    }


    function initGuardar() {
        $("#afterba-buttonGuardar").on("click", guardar);
    }


    function guardar() {
        var bar = {
            nombre: $("#afterba-textGuardar-nombre").val(),
            direccion: $("#afterba-textGuardar-direccion").val(),
        };

        afterba.service.geolocalizacion.obtenerJSONGeolocalizacion(bar).success(obtenerJSONGeolocalizacionSuccess);

        function obtenerJSONGeolocalizacionSuccess(dataJson) {
            if (dataJson.status === 'OK' && dataJson.results[0].types[0] === 'street_address') {
                bar.direccion = dataJson.results[0].formatted_address;
                bar.latitud = dataJson.results[0].geometry.location.lat;
                bar.longitud = dataJson.results[0].geometry.location.lng;
                afterba.service.bar.guardar(bar)
                        .success(function (response) {
                            alert("Guardado");
                            initMapa();
                        })
                        .fail(function (response) {
                            alert(response.status + "-" + response.statusText);
                        });
            } else {
                alert('No se pudo encontrar la direccion');
            }
        }
    }

    return {
        init: init
    };
})();
