package com.pnt.afterba.controller;

import com.pnt.afterba.domain.Bar;
import com.pnt.afterba.service.BarService;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/bares")
public class BarController {

    private static final Logger logger = Logger.getLogger(BarController.class);

    @Autowired
    private BarService barService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<Bar> obtenerBares() {
        List<Bar> bares = barService.obtenerTodos();
        return bares;
    }

    @RequestMapping(value = "/bar", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void guardar(@RequestBody Bar bar){
        try{
            barService.guardar(bar);
        }catch(IllegalArgumentException ex){
            throw new IllegalArgumentException();
        }        
    }
    
    @ResponseStatus(value=HttpStatus.NOT_ACCEPTABLE ) 
    @ExceptionHandler(IllegalArgumentException.class)
    public void conflict() {    
    }
}
