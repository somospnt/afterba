package com.pnt.afterba.repository;

import com.pnt.afterba.domain.Bar;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BarRepository extends JpaRepository<Bar, Long> {

    List<Bar> findByNombreIgnoreCaseContaining(String nombreBar);

}
