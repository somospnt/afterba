afterba.service.bar = (function () {

    function obtenerTodos() {
        var url = afterba.service.url() + "bares/";
        return afterba.service.get(url);
    }

    function guardar(bar) {
        var url = afterba.service.url() + "bares/bar";
        return afterba.service.post(url, bar);
    }

    return {
        obtenerTodos: obtenerTodos,
        guardar: guardar
    };

})();
