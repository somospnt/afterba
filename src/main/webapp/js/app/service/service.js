afterba.service = (function () {

    function get(uri) {
        return $.get(uri);
    };

    function post(uri, data) {
        return $.ajax({
            type: 'POST',
            url: uri,
            contentType: 'application/json; charset=UTF-8',
            data: JSON.stringify(data),
            dataType: 'json'
        });
    };


    function uriService() {
        url = afterba.url();
        return url + "api/";
    };

    return {
        url: uriService,
        get: get,
        post: post
    };

})();