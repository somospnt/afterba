package com.pnt.afterba.service;

import com.pnt.afterba.domain.Bar;
import java.util.List;

public interface BarService {

    void guardar(Bar bar);

    List<Bar> buscarPorNombre(String bar);
    
    List<Bar> obtenerTodos();
}
